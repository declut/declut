import statistics
import math

class normalization:
    def __init__ (self):
        pass

    def __library_sizes (self, M):   #  Genes are rows, samples are column
        sizes = [0 for _ in range (len (M[0]))]
        for gene in M:
            for sample in range (len (gene)):
                sizes[sample] += gene[sample]
        return sizes

    def __clone_structure (self, M):  #  All the rows must have the same size
        return [[0 for _ in range (len (M[0]))] for _ in M]

    def __check_gene_lengths (self, gene_lengths):
        if 0 in gene_lengths:
            raise ZeroDivisionError("genes cannot be 0bp long")

    def __get_column (self, matrix, column_index):
        return [row[column_index] for row in matrix]
    
    def __geometric_mean (self, vector, ignoreZerosNan=True):
        if ignoreZerosNan:  #  This is made to ignore 0s that are likely id expression data
            v = []
            for elem in vector:
                if elem != 0 and elem != float('nan'):
                    v.append (elem)
            return statistics.geometric_mean (v)
        else:
            return statistics.geometric_mean (vector)

    def __apply_normalization (self, M, scale_factor=1e6, scale_by_library=True, \
                               factors=None, gene_lengths=None, intround=False):
        if scale_by_library:
            sizes = self.__library_sizes (M)
        else:
            sizes = [1.0 for _ in range (len (M[0]))]

        if gene_lengths is not None:
            self.__check_gene_lengths (gene_lengths)
            gl = [gene for gene in gene_lengths]
        else:
            gl = [1.0 for _ in range (len (M))]

        if factors is not None:
            if len (factors) != (len (M[0])):
                raise TypeError("factors' vector with incompatible size")
            for i in range (len (sizes)):
                sizes[i] *= factors[i]

        table = self.__clone_structure (M)
        for gene in range (len (M)):
            for sample in range (len (M[gene])):
                table[gene][sample] = float (M[gene][sample] * scale_factor) / (sizes[sample] * gl[gene])

        if intround:
            for gene in range (len (M)):
                for sample in range (len (M[gene])):
                    table[gene][sample] = round (table[gene][sample])

        return table

    def as_round (self, M, intround=True):
        return self.__apply_normalization (M, 1, False, None, None, intround)

    def as_CPM (self, M, intround=False):
        return self.__apply_normalization (M, 1e6, True, None, None, intround)

    def as_RPKM (self, M, gene_lengths, intround=False):
        return self.__apply_normalization (M, 1e9, True, None, gene_lengths, intround)

    def as_FPKM (self, M, gene_lengths, intround=False):
        return self.__apply_normalization (M, 1e9, True, None, gene_lengths, intround)
    

    def as_TPM (self, M, gene_lengths, intround=False):
        self.__check_gene_lengths (gene_lengths)
        sizes = self.__library_sizes (M)
        tpm_table = self.__clone_structure (M)
        factors = []
        for sample in range (len (M[0])):
            factors.append (1e6 / sum ([float (M[gene][sample]) / float (gene_lengths[gene]) for gene in range (len (M))]))

        for gene in range (len (M)):
            for sample in range (len (M[gene])):
                tpm_table[gene][sample] = (float (M[gene][sample]) / gene_lengths[gene]) * factors[sample]

        if intround:
            for gene in range (len (M)):
                for sample in range (len (M[gene])):
                    tpm_table[gene][sample] = round (tpm_table[gene][sample])
                    
        return tpm_table

    def as_MOR (self, M, intround=False):  #  DeSEQ mean of ratios
        pseudo_reference = [self.__geometric_mean (gene) for gene in M]
        ratios = self.__clone_structure (M)
        for gene in range (len (M)):
            if pseudo_reference[gene] == 0:  #  All the genes counts are 0 notting to do
                continue
            for sample in range (len (M[0])):
                ratios[gene][sample] = M[gene][sample] / pseudo_reference[gene]

        factors = []
        for sample in range (len (M[0])):
            factors.append (statistics.median (self.__get_column (ratios, sample)))

        return self.__apply_normalization (M, 1, False, factors, None, intround)
    
    #  This is used also by TMM to decide the reference sample
    def __quartile_normalization_factors (self, M, method, normalize_by_size=False):
        m = []   #   Remove genes with 0 for all samples
        for gene in range (len (M)):
            if sum (M[gene]) != 0:
                m.append (M[gene])
                
        factors = []
        for sample in range (len (M[0])):
            q = statistics.quantiles (self.__get_column (m, sample), n=4, method="inclusive")
            # method = 1 --> median, method = 2 -->  75' percentile
            factors.append (q[method])

        #  This is not used in upper quartile normalization but is necessary for TMM reference selection
        if normalize_by_size: 
            sizes = self.__library_sizes (M)
            for sample in range (len (M[0])):
                factors[sample] /= sizes[sample]
                
        mean = statistics.mean (factors)
        for sample in range (len (factors)):
            factors[sample] = factors[sample] / mean
            
        return factors

    #  As betweenLaneNormalization function in R
    def as_UQ (self, M, intround=False):
        factors = self.__quartile_normalization_factors (M, 2, False)
        return self.__apply_normalization (M, 1, False, factors, None, intround)

    def as_MQ (self, M, intround=False):
        factors = self.__quartile_normalization_factors (M, 1, False)
        return self.__apply_normalization (M, 1, False, factors, None, intround)

    # proportion is in the range 0-1. default would be 0.3 for m and 0.05 for a
    def __get_untrimmed_gene_ids (self, values, proportion):
        not_nan_genes = []
        for i in range (len (values)):
            if values[i] == values[i]:   # genes not nan - 0 count in sample or reference
                not_nan_genes.append ([values[i], i]) 
        not_nan_genes.sort ()
        low_threshold = math.floor (len (not_nan_genes) * proportion) + 1
        high_threshold = len (not_nan_genes) - low_threshold + 1
        return set ([not_nan_genes[i][1] for i in range (low_threshold - 1, high_threshold)])

    def __TMM_normalization_factors (self, M):
        refQ =  self.__quartile_normalization_factors (M, 2, True)  #  2 --> 75' percentile
        # find that closer sampleto the mean - XXX refQ was divided to the mean thus it is argmin (abs (1 - value))
        refQ = [abs (1 - elem) for elem in refQ]
        refIndex = 0
        while refQ[refIndex] != min (refQ):
            refIndex += 1
        #  Compute M-values and A-values  of the paper
        sizes = self.__library_sizes (M)
        scaled_M = self.__clone_structure (M)
        for gene in range (len (M)):
            for sample in range (len (M[gene])):
                scaled_M[gene][sample] = M[gene][sample] / sizes[sample]
        m = self.__clone_structure (M)
        a = self.__clone_structure (M)
        w = self.__clone_structure (M)
        for gene in range (len (M)):
            if M[gene][refIndex] != 0:   #  Not used otherwise
                w_ref = (sizes[refIndex] - M[gene][refIndex]) / (sizes[refIndex] * M[gene][refIndex])
            for sample in range (len (M[gene])):
                if M[gene][sample] == 0 or M[gene][refIndex] == 0:
                    m[gene][sample] = float ("nan")
                    a[gene][sample] = float ("nan")
                    w[gene][sample] = float ("nan")
                elif sample == refIndex: # M-values of reference with itself is not used bat would be 1
                    m[gene][sample] = 0
                    a[gene][sample] = math.log (scaled_M[gene][sample] * scaled_M[gene][refIndex], 2) / 2
                    w[gene][sample] = w_ref + w_ref
                else: 
                    m[gene][sample] = math.log (scaled_M[gene][sample] / scaled_M[gene][refIndex], 2)
                    a[gene][sample] = math.log (scaled_M[gene][sample] * scaled_M[gene][refIndex], 2) / 2
                    w[gene][sample] = ((sizes[sample] - M[gene][sample]) / (sizes[sample] * M[gene][sample])) + w_ref

        factors = []
        for sample in range (len (M[0])):
            m_ids = self.__get_untrimmed_gene_ids (self.__get_column (m, sample), 0.3)
            a_ids = self.__get_untrimmed_gene_ids (self.__get_column (a, sample), 0.05)
            overall_m = 0.0
            overall_w = 0.0
            for gene in m_ids & a_ids:  #  The intersection
                overall_m += (m[gene][sample] / w[gene][sample])
                overall_w += (1 / w[gene][sample])

            factors.append (2 ** (overall_m / overall_w))
        gm = self.__geometric_mean (factors, ignoreZerosNan=False)
        #  These are edgeR factors
        return [factor / gm for factor in factors]

    def as_TMM_CPM (self, M, intround=False):
        factors = self.__TMM_normalization_factors (M)
        return self.__apply_normalization (M, 1e6, True, factors, None, intround)

    def as_TMM_FPKM (self, M, gene_lengths, intround=False):
        factors = self.__TMM_normalization_factors (M)
        return self.__apply_normalization (M, 1e9, True, factors, gene_lengths, intround)

    def as_TMM_RPKM (self, M, gene_lengths, intround=False):
        factors = self.__TMM_normalization_factors (M)
        return self.__apply_normalization (M, 1e9, True, factors, gene_lengths, intround)

