# reference here: https://biocorecrg.github.io/CRG_RIntroduction/volcano-plots.html

if (require("ggplot2") == FALSE) install.packages("ggplot2")
if (require("ggrepel") == FALSE) install.packages("ggrepel")
library (ggplot2)
library(ggrepel)
args = commandArgs(trailingOnly=TRUE)
if (length(args) == 0) {
  stop("At least one argument must be supplied (input file).", call.=FALSE)
}
LFC_T <- if (length(args) >= 2) c(-as.double(args[2]), as.double(args[2])) else c(-1, 1)
score_T <- if (length(args) >= 3) 1 - as.double(args[3]) else  1 - ((0.7+0.7)/2)  #  This is just 0.3 but shows why

orientation <- "landscape"
orientation <- "portrait"
outfile = paste (args[1], "pdf", sep=".")
table <- read.table(args[1], header = T, sep = "\t")
de <- table[, c("Gene", "LFC")]
de$score <- 1 - ((table[, "F1"] + table[, "disp."]) / 2)

de$Expression <- "NO"
de$Expression[de$LFC > LFC_T[2] & de$score < score_T] <- "UP"
de$Expression[de$LFC < LFC_T[1] & de$score < score_T] <- "DOWN"

de$delabel <- NA
de$delabel[de$Expression != "NO"] <- de$Gene[de$Expression != "NO"]



mycolors <- c("blue", "red", "gray")
names(mycolors) <- c("DOWN", "UP", "NO")
pdf (outfile, height=9, width=16)
if (orientation == "portrait") {
p <- ggplot(data=de, aes(x=LFC, y=-log10(score), col=Expression, label=delabel)) +
  geom_point() + 
  theme_minimal() +
  geom_text_repel() +
  geom_vline(xintercept=LFC_T, col="black", linetype = "longdash") +
  geom_hline(yintercept=-log10(score_T), col="black", linetype = "longdash") +
  scale_colour_manual(values = mycolors) + expand_limits(y=0)
} else {  #  landscape
p <- ggplot(data=de, aes(y=LFC, x=-log10(score), col=Expression, label=delabel)) + 
  geom_point() + 
  theme_minimal() +
  geom_text_repel() +
  geom_hline(yintercept=LFC_T, col="black", linetype = "longdash") +
  geom_vline(xintercept=-log10(score_T), col="black", linetype = "longdash") +
  scale_colour_manual(values = mycolors) + expand_limits(x=0)
}
p
dev.off ()