import os

unavailable_formats = set ()

try:
    import openpyxl
except ImportError:
    unavailable_formats.add ("xlsx")

try:
    import xlrd
except ImportError:
    unavailable_formats.add ("xls")

try:
    import csv
except ImportError:
    unavailable_formats.add ("csv")
    
class file_reader:
    def __init__ (self, fileName=None):
        self.filename = fileName
        self.errorString = ""
        self.reset ()
   
    def reset (self):  #  Used to reset values and prepare to read anothe file
        self._need_to_transpose = None  # Will guess if it is necessary
        self._length_index = -1    #  if gene length is in the file this is the row/column index
        self._class_index = -1     #  if target is in the file this is the row/column index
        self.skip_rows = []        #  0-based index of the rows to skip
        self.skip_columns = []     #  0-based index of the columns to skip
        self._gene_index = -1      #  index of the row/column with gene name - if any
        self._sample_index = -1    #  index of the row/column with sample name - if any
        self.errorString = ""      #  Set when an exception is raised
        
    def rows_as_genes (self):      #  the input matrix has genes as rows and samples as columns
        self._need_to_transpose = False
    def rows_as_samples (self):    #  the input matrix has genes as columns and samples as rows
        self._need_to_transpose = True
    def column_as_genes (self):    #  the input matrix has genes as columns and samples as rows
        self._need_to_transpose = True
    def column_as_samples (self):  #  the input matrix has genes as rows and samples as columns
        self._need_to_transpose = False        

    def __position_to_index (self, position):  # can be 1-based number of excel column letter
        if position is None:
            return -1
        if isinstance (position, str):
            pos = 0
            for char in position:
                pos = pos * 26
                pos = pos + (ord (char) - ord ('A') + 1)
            return pos - 1
        else:
            if position <= 0:
                return -1
            return position - 1
        
    def SampleNamePosition (self, position):  # can be 1-based number of excel column letter
        self._sample_index = self.__position_to_index (position)

    def geneNamePosition (self, position):  # can be 1-based number of excel column letter
        self._gene_index = self.__position_to_index (position)

    def geneLengthPosition (self, position):  # can be 1-based number of excel column letter
        self._length_index = self.__position_to_index (position)

    def sampleClassPosition (self, position):  # can be 1-based number of excel column letter
        self._class_index = self.__position_to_index (position)

    def setSkipRows (self, row_positions = []): #  1-based index or excel column
        self.skip_rows = []
        for position in row_positions:
            self.skip_rows.append (self.__position_to_index (position))

    def setSkipColumns (self, column_positions = []): #  1-based index or excel column
        self.skip_columns = []
        for position in column_positions:
            self.skip_columns.append (self.__position_to_index (position))

    def __discard_extra_rows_cols (self, M, rowslist, columnslist):
        new_M = []
        for i in range (len (M)):
            if i not in rowslist:
                new_M.append ([M[i][j] for j in range (len (M[i])) if j not in columnslist])
        return new_M

    def __read_row (self, M, index, columnslist):
        return [M[index][j] for j in range (len (M[index])) if j not in columnslist]

    def __read_column (self, M, index, rowslist):
        return [M[i][index] for i in range (len (M)) if i not in rowslist]

    def __filename (self, filename):
        if filename is None and self.filename is None:
            self.errorString = "filename nos specified"
            raise NameError(self.errorString)
        if filename is not None:
            if not os.path.exists (filename):
                self.errorString = filename + " does not exist"
                raise NameError(self.errorString)
            return filename
        else:
            if not os.path.exists (self.filename):
                self.errorString = self.filename + " does not exist"
                raise NameError(self.errorString)
            return self.filename

    def __guess_traspose (self, tot_rows, tot_columns):
        if self._need_to_transpose is None:  # Assumes there are more genes than samples
            if tot_columns > tot_rows:  #  Genes should be on columns
                return True
            else:
                return False
        else:
            return self._need_to_transpose
        
    def __transpose (self, matrix):
        max_column = max ([len (row) for row in matrix])
        M = [[] for _ in range (max_column)]
        for row in matrix:
            for i in range (max_column):
                M[i].append (row[i])
        return M
    
    def __convert (self, value, impute_zeros=True):
        if value == None and impute_zeros:
            return 0
        if not isinstance(value, str):
            return value
        if impute_zeros:
            if value == "NA":
                return 0
            if value == "na":
                return 0
            if value == "Na":
                return 0
            if value == "":
                return 0
        if value == "":
            return value
        if value.isdecimal ():
            return int (value)
        if value[0].isdecimal () and value[1:].replace ('.','',1).isdecimal ():
            return float (value)
        return value
    
    def __fill (self, M):
        for i in range (len (M)):
            for j in range (len (M[i])):
                M[i][j] = self.__convert (M[i][j])
        return M
        
    def __parse_matrix (self, M, tot_rows, tot_columns):
        if self.__guess_traspose (tot_rows, tot_columns): #  (rows/columns) samples/genes
            M = self.__transpose (M)
            skip_columns = [row for row in self.skip_rows]
            skip_rows = [column for column in self.skip_columns]
        else:   #  (rows/columns) genes/samples
            skip_rows = [row for row in self.skip_rows]
            skip_columns = [column for column in self.skip_columns]
        M = self.__fill (M)
        #  From here on the matrix is rows -> genes columns -> samples
        skip_columns.append (self._gene_index)
        skip_columns.append (self._length_index)
        skip_rows.append (self._class_index)
        skip_rows.append (self._sample_index)
        if self._class_index != -1:
            class_labels = self.__read_row (M, self._class_index, skip_columns)
        else:
            class_labels = []            
        if self._gene_index != -1:
            gene_labels = self.__read_column (M, self._gene_index, skip_rows)
        else:
            gene_labels = []            
        if self._length_index != -1:
            gene_lengths = self.__read_column (M, self._length_index, skip_rows)
        else:
            gene_lengths = []            
        M = self.__discard_extra_rows_cols (M, skip_rows, skip_columns)
        return M, gene_labels, class_labels, gene_lengths
    
    def read_xlsx (self, fileName=None, sheetName=None):
        if "xlsx" in unavailable_formats:
            self.errorString = "xlsx not supported on your device. Try pip3 install xlrd to fix this issue"
            raise RuntimeError(self.errorString)
        book = openpyxl.load_workbook (self.__filename (fileName), read_only=True,
                                           keep_vba=False, data_only=True, keep_links=False,
                                           rich_text=False)
        if sheetName is None:
            sheet = book.active
        else:
            sheet_id = -1
            for i in range (len (book._sheets)):
                if book._sheets[i].title == sheetName:
                    sheet_id = i
                    break
            if sheet_id == -1:  #  sheet not found - raise a error
                sheets_names = ", ".join ([b.title for b in book._sheets])
                self.errorString = sheetName + " not found, possible options are " + sheets_names
                raise IndexError(self.errorString)
            sheet = book._sheets[sheet_id]

        #  Guess if the matrix is (rows/columns) genes/samples - False or samples/genes - True
        tot_columns = sheet.max_column - sheet.min_column
        tot_rows = sheet.max_row - sheet.min_row
        #  Read data from the sheet
        M = [] 
        for row in sheet.rows:         
            r = [cell.value for cell in row]
            M.append (r)

        return self.__parse_matrix (M, tot_rows, tot_columns)

    def read_xls (self, fileName=None, sheetName=None):
        if "xls" in unavailable_formats:
            self.errorString = "xls not supported on your device. Try pip3 install openpyxl to fix this issue"
            raise RuntimeError(self.errorString)
        book = xlrd.open_workbook(self.__filename (fileName))
        
        if sheetName is None:
            sheet = book.sheet_by_index(0)
        else:
            sheet_names = book.sheet_names ()
            sheet_id = -1
            for i in range (book.nsheets):
                if sheet_names[i] == sheetName:
                    sheet_id = i
                    break
            if sheet_id == -1:  #  sheet not found - raise a error
                self.errorString = sheetName + " not found, possible options are: " + ", ".join (sheet_names)
                raise IndexError(self.errorString)
            sheet = book.sheet_by_index (sheet_id)

        #  Guess if the matrix is (rows/columns) genes/samples - False or samples/genes - True
        tot_columns = sheet.ncols
        tot_rows = sheet.nrows
        #  Read data from the sheet
        M = [[None for _ in range (tot_columns)] for _ in range (tot_rows)]
        
        for i in range (tot_rows):         
            for j in range (tot_columns):
                M[i][j] = sheet.cell_value (i,j)

        return self.__parse_matrix (M, tot_rows, tot_columns)
       
    def read_csv (self, fileName=None):
        if "csv" in unavailable_formats:
            self.errorString = "csv not supported on your device. Try pip3 install csv to fix this issue"
            raise RuntimeError(self.errorString)
        csvfile = open(self.__filename (fileName), newline='')
        dialect = csv.Sniffer().sniff(''.join(csvfile.readline() for _ in range(10)))
        csvfile.seek(0)
        reader = csv.reader(csvfile, dialect)
        tot_rows = 0
        tot_columns = 0
        M = [] 
        for row in reader:
            r = [elem for elem in row]
            M.append (r)
            tot_rows += 1
            if len (r) > tot_columns:
                tot_columns = len (r)
        csvfile.close ()
        #  Pad the matrix to make it rectangular
        for i in range (tot_rows):
            while len (M[i]) < tot_columns:
                M[i].append (None)
            
        return self.__parse_matrix (M, tot_rows, tot_columns)

    
