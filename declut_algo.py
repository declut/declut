import math
from operator import itemgetter
import statistics

class declut_algo:
    def __init__ (self, threshold_function, progress_step_helper=None, progress_clean_helper=None):
        self.results = []
        self.set_threshold_function (threshold_function)
        if progress_step_helper is None:
            self.progress_step_helper = self.__progress_print_step
        else:
            self.progress_step_helper = progress_step_helper
        if progress_clean_helper is None:
            self.progress_clean_helper = self.__progress_cleanup
        else:
            self.progress_clean_helper = progress_clean_helper
            
    def set_progress_step_helper (self, progress_step_helper=None):  #  Call without parameters to disable progressbar
        self.progress_step_helper = progress_step_helper

    def set_progress_clean_helper (self, progress_clean_helper=None):#  Call without parameters to disable progressbar cleanup
        self.progress_clean_helper = progress_clean_helper
        
    def set_threshold_function (self, threshold_function):
        if threshold_function == "DD":  
            self.__compute_threshold = self.__compute_threshold_diametral_distance
        elif threshold_function == "PM":  
            self.__compute_threshold = self.__compute_threshold_point_mean
        elif threshold_function == "MS": 
            self.__compute_threshold = self.__compute_threshold_mean_shift
        elif threshold_function == "DM":  
            self.__compute_threshold = self.__compute_threshold_diametral_mean
        elif threshold_function == "DMM":  
            self.__compute_threshold = self.__compute_threshold_diametral_median
        else:   #   Impossible because the command line prevent this
            self.__compute_threshold = self.__compute_threshold_diametral_median

            
    def __intracluster_distance (self, group):  # Distance among elements of a cluster
        if len (group) <= 1:  #  avoid 0 division if there is zero or only one element in the group
            return 0
        distance = 0.0
        for i in range (len (group)):
            for j in range (i):
                if group[i] > group[j]:
                    distance += (group[i] - group[j])
                else:
                    distance += (group[j] - group[i])
        return distance / ((( len (group)**2) - len (group)) / 2)

    def __split_elements (self, v, threshold):
        if threshold == 0:  #   if threshold is 0 append all elements to one group
            return [[elem for elem in v], []]
        groups = [[], []]
        for elem in v:
            if elem >= threshold:
                groups[1].append (elem)
            else:
                groups[0].append (elem)
        return groups

    def __avrg_dist (self, elem, group, belongs=True):
        d = 0.0
        if len (group) == 0 and belongs:
            assert ("Cannot belong to an empty group")
        if len (group) == 0:  #   Distance of an element with an empty group is infinity
            return math.inf
        if len (group) == 1 and belongs:
            return 0
        for j in range (len (group)):
            if elem >  group[j]:
                d += (elem - group[j])
            else:
                d += (group[j] - elem)
        if belongs:
            return d / (len (group) - 1)
        else:
            return d / len (group)

    def __compute_threshold_diametral_distance (self, v, dummy): # distance between the diametral point
        return ((max (v) - min (v)) / 2) + min (v)

    def __compute_threshold_point_mean (self, v, dummy): # average value of the points
        return statistics.mean (v)

    def __get_nearest_neighbours (self, v, val, k):
        d = [[abs (val - vi), vi] for vi in v]
        d.sort ()
        return [d[i][1] for i in range (k)]

    #  This procedure makes no sense if I just iterate once
    def __compute_threshold_mean_shift (self, v, cardinality_smaller_class):
        sv = sorted (v)
        means = [sv[i] for i in range (len (sv))]
        new_means = [None for i in range (len (sv))]
        #  Apply the mean shift correction
        for loop in range (3):   #  No idea of how many itarations should I make
            for i in range (len (means)):
                nn = self.__get_nearest_neighbours (means, means[i], cardinality_smaller_class)
                new_means[i] = (sum (nn) / cardinality_smaller_class)
            #  Update means all at once
            means = [new_means[i] for i in range (len (new_means))]
        #  Compute the threshold as the mid point of the diametral points - XXX remember are sorted
        return ((means[-1] - means[0]) / 2) + means[0]

    # Compute the threshold as the mid point of the diametral means
    def __compute_threshold_diametral_mean (self, v, cardinality_smaller_class):
        sv = sorted (v)
        means = [statistics.mean (sv[:cardinality_smaller_class]), statistics.mean (sv[-cardinality_smaller_class:])]
        return (((means[-1] - means[0]) / 2) + means[0]) 

    # Compute the threshold as the mid point of the diametral medians XXX  this is that of the paper
    def __compute_threshold_diametral_median (self, v, cardinality_smaller_class):
        sv = sorted (v)
        median = [statistics.median (sv[:cardinality_smaller_class]), statistics.median (sv[-cardinality_smaller_class:])]
        return (((median[-1] - median[0]) / 2) + median[0]) 


    def __do_clustering (self, v, cardinality_smaller_class):
        threshold = self.__compute_threshold (v, cardinality_smaller_class)
        groups = self.__split_elements (v, threshold)
        distances = [self.__intracluster_distance (groups[i]) for i in range (2)]
        toSwitch = [[False for i in range (len (groups[0]))], [False for i in range (len (groups[1]))]]
        need_switch = [1,1]  #  fake just to enter in the loop
        #  Qui andra' il loop
        while max (need_switch) != 0:
            need_switch = [0,0]
            for cl in range (2):
                oth_cl = (cl +1) % 2
                for i in range (len (groups[cl])):
                    if toSwitch[cl][i] == False:
                        if self.__avrg_dist (groups[cl][i], groups[cl]) > self.__avrg_dist (groups[cl][i], groups[oth_cl], False):
                            toSwitch[cl][i] = True
                            need_switch[cl] += 1
            if min (need_switch) != 0: # elements of both clusters should be moved - impossible
                assert ("Problemone")
            for cl in range (2):
                oth_cl = (cl +1) % 2
                if need_switch[cl] != 0:  #  this cluster contains elements to move
                    for i in range (len (groups[cl]) -1, -1, -1):  # scan backword
                        if toSwitch[cl][i] == True:
                            groups[oth_cl].append (groups[cl][i])  # copy to the other group
                            toSwitch[oth_cl].append (None)          # mark as no longer movable
                            del groups[cl][i]    # delete from the source group
                            del toSwitch[cl][i]   # delete from the source group
                    for i in range (len (groups[oth_cl])):  #  if elements move to this cluster cannot go back
                        toSwitch[oth_cl][i] = None
        #end-while
        #  If all elements stand in the same cluster don't recompute the threshold
        if min (len (groups[0]), len (groups[1])) == 0:  
            return threshold
        threshold = (max (min (groups[0]), min (groups[1])) - min (max (groups[0]), max (groups[1]))) / 2
        threshold += min (max (groups[0]), max (groups[1]))
        #print (threshold)
        return threshold

    def __return_under_threshold_class_id (self, v, threshold, gt):
        class_count = [0,0]
        for i in range (len (v)):
            if v[i] < threshold:
                class_count[gt[i]] += 1
        if class_count[0] > class_count[1]:
            return 0
        else:
            return 1

    def __measures_to_clustering (self, v, threshold, under_class_id):
        c = []
        over_class_id = 1 - under_class_id
        for i in range (len (v)):
            if v[i] < threshold:
                c.append (under_class_id)
            else:
                c.append (over_class_id)
        return c

    def __measures_dispersion (self, v, threshold, under_class_id, gt, precision_round=3):
        over_class_id = 1 - under_class_id
        max_dist = max ([abs (v[i] - threshold) for i in range (len (v))])
        tot_wrong = 0
        accumulator = 0
        for i in range (len (v)):
            #  The case v[i] == threshold should be impossible
            if v[i] < threshold and gt[i] != under_class_id:
                accumulator += abs (v[i] - threshold)
                tot_wrong += 1
            if v[i] > threshold and gt[i] != over_class_id:
                accumulator += abs (v[i] - threshold)
                tot_wrong += 1
        if accumulator == 0:   #  This is the case where all elements are in the right cluster
            return 1
        return  round (1 - (accumulator / (tot_wrong * max_dist)), precision_round)

    def __FoldChange (self, counts, cluster, precision_round):  #  class 0 (control) vs class 1 (test)
        h = hn = 0   
        d = dn = 0
        for i in range (len (counts)):
            if cluster[i] == 0: #  control samples
                h += counts[i]
                hn += 1
            else:               #  test samples
                d += counts[i]
                dn += 1
        if h == 0:  #   border line case where average is 0 or no elem is classified as control
            h = 0.000001
        else:
            h = float (h) / hn
        if d == 0: #   border line case where average is 0 or no elem is classified as test
            d = 0.000001
        else:
            d = float (d) / dn
        return round (h, 2), round (d, 2), round (h/d, precision_round), \
            round (math.log2 (h / d), precision_round), round (abs (math.log2 (h / d)), precision_round)

    def __confusion_matrix (self, gt, cluster):
        m = [[0, 0], [0, 0]]
        for i in range (len (gt)):
            m[gt[i]][cluster[i]] += 1
        return m

    def __get_GT (self, class_labels):
        self.gt = []
        for label in class_labels:
            if label == self.main_class:
                self.gt.append (0)
            else:
                self.gt.append (1)
                
    def __get_smallest_class_size (self):
        self.smallest_class_size = min (sum(self.gt), len (self.gt) - sum(self.gt))

    def prepare_result_header (self, verbose=True):
        if verbose:
            return ["Gene", "F1", "disp.", self.main_class, self.other_class, "FC", "LFC", "threshold"]
        return ["Gene", "F1", "disp.", "LFC"]
                
    #  Return a copy of self.results with only the required fields, after applying filters and ordering
    def prepare_result_table (self, verbose=True, filtering=None, ordering=None):
        result_table = []
        result_row_length = len (self.results[0])
        if filtering is None:
            filtering = []
        if ordering is None:
            ordering = []
        # Name, f1, dispersion, h, d, fc, lfc, abs(lfc), threshold
        fields2print = [True, True, True, True, True, True, True, False, True]
        if not verbose:
            fields2print = [True, True, True, False, False, False, True, False, False]
 
        field2index = {"F1": 1, "dispersion" : 2, "LFC" : 6, "ABSLFC": 7}

        for elem in self.results:
            to_filter = False
            for condition in filtering:
                if elem[field2index[condition[0]]] < condition[1]:
                    to_filter = True
                    break
            if to_filter:
                continue
            result_table.append ([elem[i] for i in range (result_row_length)])
            
        for elem in ordering[::-1]:
            result_table.sort (key=itemgetter(field2index[elem]), reverse=True)

        for row in range (len (result_table)):
            result_table[row] = [result_table[row][i] for i in range (result_row_length) if fields2print[i]]

        return result_table
   
    def __process_gene (self, gene_counts, gene_label, gt, smallest_class_size, precision_round=3):
        threshold = self.__do_clustering (gene_counts, smallest_class_size)
        under_class_id = self.__return_under_threshold_class_id (gene_counts, threshold, gt)
        cluster = self.__measures_to_clustering (gene_counts, threshold, under_class_id)
        cm = self.__confusion_matrix (gt, cluster)
        #  F1-score
        c1 = cm[0][0] / (cm[0][0] + 0.5 * (cm[0][1] + cm[1][0]))
        c2 = cm[1][1] / (cm[1][1] + 0.5 * (cm[0][1] + cm[1][0]))
        fold_change = self.__FoldChange (gene_counts, cluster, precision_round)
        # Name, f1, dispersion, h, d, fc, lfc, abs(lfc), threshold
        return [gene_label, round (((c1 + c2) / 2.0), precision_round), \
                self.__measures_dispersion (gene_counts, threshold, under_class_id, gt, precision_round), fold_change[0], \
                fold_change[1], fold_change[2], fold_change[3], fold_change[4], round (threshold, 2)]

    def __progress_print_step (self, current_elem, total_elems):
        #  Print progress bar
        percentage = int ((current_elem * 100) / total_elems) 
        progress_bar = "#" * int (percentage / 2)
        remaining_bar = " " * (50 - int (percentage / 2))
        print(f"\rProgress: {progress_bar}{remaining_bar} | {percentage}%", end="")

    def __progress_cleanup (self):
        #  Clean up the progress bar
        print(f"\r                                                                    ", end="")  
        print(f"\r", end="")  
        
    def declut (self, M, gene_labels, class_labels, main_class=None):
        self.results = []
        if main_class is None:   #  If not specified use that of the first sample
            self.main_class = class_labels[0]
        else:
            self.main_class = main_class
        self.other_class = [elem for elem in set (class_labels) if elem != self.main_class][0]
        self.__get_GT (class_labels)
        self.__get_smallest_class_size ()
        for gene in range (len (M)):
            #  Print progress bar
            if self.progress_step_helper is not None:
                self.progress_step_helper (gene + 1, len (M))
            r = self.__process_gene (M[gene], gene_labels[gene], self.gt, self.smallest_class_size)
            self.results.append (r)
        #  Clean up the progress bar
        if self.progress_clean_helper is not None:
            self.progress_clean_helper ()


