#!/usr/bin/env python3

from reader import file_reader 
from normalization import normalization
from command import command_line
from declut_algo import declut_algo
import sys

def load (fr, section):
    fr.reset ()
    if section.gene_index is not None:
        fr.geneNamePosition (section.gene_index)
    if section.length_index is not None:
        fr.geneLengthPosition (section.length_index)
    if section.class_index is not None:
        fr.sampleClassPosition (section.class_index)
    if section.sample_index is not None:
        fr.SampleNamePosition (section.sample_index)
    fr.setSkipColumns ([elem for elem in section.skip_columns])
    fr.setSkipRows ([elem for elem in section.skip_rows])
    #  need_to_transpose can be True, False or None. In case of None we don't need to call anything
    if section.need_to_transpose == True:
        fr.rows_as_samples ()
    if section.need_to_transpose == False:
        fr.rows_as_genes ()
    if section.filetype == "csv":
        return fr.read_csv (section.filename)
    elif section.filetype == "xls":
        return fr.read_xls (section.filename, section.sheet)
    elif section.filetype == "xlsx":
        return fr.read_xlsx (section.filename, section.sheet)
    else:
        raise AttributeError("file format not supported")


def display_results (declut_obj, filename=None, header=True, verbose=True, filtering=None, ordering=None):
    result_header = declut_obj.prepare_result_header (verbose)
    result_table = declut_obj.prepare_result_table (verbose, filtering, ordering)
    if filename is not None:
        pf = open (filename, "w")
    if header:
        if filename is not None:
            pf.write ("\t".join (result_header) + "\n")
        else:
            print ("\t".join (result_header))
    for elem in result_table:
        for i in range (1, len (elem)):
            elem[i] = str (elem[i])
        if filename is not None:
            pf.write ("\t".join (elem) + "\n")
        else:
            print ("\t".join (elem))
    if filename is not None:
        pf.close ()

if __name__ == "__main__":
    cl = command_line ()
    cl.parse_args ()
    fr = file_reader ()
    #cl.sections[0].print_params ("Gene")
    #cl.sections[1].print_params ("Sample")
    #cl.sections[2].print_params ("Lengths")
    try:
        M, gene_labels, class_labels, gene_lengths = load (fr,  cl.sections[0])
        #print (class_labels)
        if cl.sections[1].filename is not None:
            _, _, class_labels, _ = load (fr,  cl.sections[1])
        if cl.sections[2].filename is not None:
            _, _, _, gene_lengths = load (fr,  cl.sections[2])
    except:
        cl.print_error (fr.errorString)
    #print (class_labels)
    #  This function does not return in case of error
    cl.check_file_content (M, gene_labels, class_labels, gene_lengths)
    
    n = normalization ()
    if cl.normalization == "NO":
        NM = n.as_round (M, cl.round)
    elif cl.normalization == "CPM":
        NM = n.as_CPM (M, cl.round)
    elif cl.normalization == "RPKM":
        NM = n.as_RPKM (M, gene_lengths, cl.round)
    elif cl.normalization == "FPKM":
        NM = n.as_FPKM (M, gene_lengths, cl.round)
    elif cl.normalization == "TPM":
        NM = n.as_TPM (M, gene_lengths, cl.round)
    elif cl.normalization == "MOR":
        NM = n.as_MOR (M, cl.round)
    elif cl.normalization == "UQ":
        NM = n.as_UQ (M, gene_lengths, cl.round)
    elif cl.normalization == "MQ":
        NM = n.as_MQ (M, gene_lengths, cl.round)
    elif cl.normalization == "TMM":
        if len (gene_lengths) == 0:
            NM = n.as_TMM_CPM (M, cl.round)
        else:
            NM = n.as_TMM_RPKM (M, gene_lengths, cl.round)

    #print (len (NM))
    #print (len (gene_labels))
    #print (len (class_labels))
    #print (len (gene_lengths))

    d = declut_algo (cl.threshold_function)
    
    if cl.quiet:
        d.set_progress_step_helper ()  #  Call without parameters to disable progressbar
        d.set_progress_clean_helper () #  Call without parameters to disable progressbar cleanup

    d.declut (NM, gene_labels, class_labels)
    display_results (d, cl.outfile, cl.no_out_header, cl.verbose_output, cl.filter, cl.sort)

    
