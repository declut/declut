# DeClUt: decluttering differentially expressed genes through clustering of their expression profiles

DeClUt is a non-parametric methods to find differentially expressed
genes from a RNA-seq experiment inspired to data clustering and based
on the intuitive idea that a gene is differentially expressed if its
expression levels among samples partition into two reasonably compact
and well-separated groups corresponding to the two conditions under
study. 


# Install
DeClUt doesn't actually need to be installed as it is pure python3
script (DeClUt.py) leveraging only on some utilities stored in the other
.py files. You just need to copy all the .py files in a convenient
directory and call `DeClUt.py`

The **declut.1** file contains the manual for DeClUt. If you don't have
root permission you can see it using the command `man ./declut.1` from
the same directory of the file. With root permission you can copy
**declut.1** to the `/usr/share/man/man1` directory and see the manual
with the usual `man declut` command. 

To hundle *xls*,  *xlsx* and *csv* files DeClUt leverage on common
python3 libraries. To install them it is possible to use the *pip3*
command from python.

- `pip3 install xlrd` will install xlrd library for *xls* files
- `pip3 install openpyxl` will install openpyxl library for *xlsx* files
- `pip3 install csv`  will install csv library for comma/tab/space
  separated files.

# Usage

DeClUt takes as input a matrix with the quantification of each gene
for each sample. DeClUt allows the count matrix to be stored either as
2Genes/Samples or Samples/Genes. DeClUt guesses the orientation of the
matrix supposing that the experiment involves more genes than
samples. If this is not the case (this can happen for example with
miRNAs for large cohorts like those of TCGA) it is possible to let
DeClUt know the correct orientation using the `-O` option. 

In addition, DeClUt requires to know the experimental condition
associated to each sampls. This information can be stored in a separated
file (specified through the `-fs` option) or in the same file of the
count matrix. In this latter case the index of the row/column
containing this information can be specified through the
`--sampletype` (`-t`) option. 

Some normalization methods require to know the length of each
gene. Again this information can be stored i a separated file
(specified through the `-fl` option or in the same file of the count
matrix (see the `--genelength`  option).

When stored in separated files both the experimental conditions and
the gene lengths are supposed to be column-wise (If this is not the
case it is possible to use the `-O` option to specify the correct
orientation). Otherwise, experimental conditions are supposed to have
the same orientation of samples and gene lengths to have the same
orientation of genes.

DeClUt implements some of the most common data normalization methods
that can be specified with the option `-n` (or `--normalization`)

* CPM: Counts per Million
* RPKM Reads per kilobase Million (Requires gene lengths)
* FPKM Fragments per kilobase Million (Requires gene lengths)
* TPM Transcripts per Million (Requires gene lengths)
* MOR Mean of ratios (similar to the DeSEQ2 R package)
* UQ Upper quartile normalization (similar to betweenLaneNormalization
with *which* parameter set to *upper* from EDASeq R package)
* MQ median normalization (similar to betweenLaneNormalization from
with *which* parameter set to *median* from EDASeq R package)
* TMM trimmed mean of M values (similar to TMM from edgeR R package).
In this case, if gene lengths are provided, after library scaling, the FPKM
normalization is applied, otherwise CPM is applied.


##  Output

DeClUt returns a table where each row corresponds to a gene. By
default the table reports only the gene name, the F1 score, the
dispersion index and the log fold-change. Specifying the option
`--verbose-output` (in short `-v`) extra information is added:

* **Gene:** the gene name as specified in the count matrix;
* **F1:** the f1 score, in the range [0-1], measures the coherence of
  the clustering with the two campared cohorts of samples;
* **disp.:** the dispersion index, in the range [0-1], measures the
  divergence of misclassified samples from their correct class. Higher
  values indicate better convergence.
* **Control class:**  the average count of samples clustered as
  belonging to the control class;
* **Test class:** the average count of samples clustered as
  belonging to the test class;
* **FC:** fold-change of the control class vs the test class. Values
  higher than 1 indicate over-epxression of the control class compared
  to the test class;
* **LFC:** base 2 logarithm of the fold-change;
* **threshold:**  count value used by the clustering algorithm to
  separate elements belonging to the control class and elements
  belonging to the test class.

DeClUt offers sorting and filtering options to arrange the results
table. When using the `-S` (or `--sort`) parameter, the table will be
sorted according to the selected field (F1, dispersion, LFC, or
ABSLFC). This option may be specified multiple times. 
The ordering of `-S` instances is significant, meaning that `-S F1 -S
LFC` will result in a distinct result from `-S LFC -S F1`. The sorting
is consistently executed in a decreasing order of the specified field. 
Results can be filtered using the `--filter` option. Similarly to
sorting, DeClUt allows multiple occurrences of this option. In this
case, however, the final result is not affected by the order. Each
instance of the `--filter ` parameter takes two arguments: the field 
(F1 , LFC or ABSLFC) and a threshold. Results under the threshold are
discarded. 

## Handling files
We made our best to make DeClUt ready to handle your data in their current
form easing you from frustrating format conversions. The disadvantage
of this is that the command line can appear complex at first
glance. Some options, for example, can be repeated in different
positions of the command line with different behaviour.
The idea is that the command line is divided in sections (one for each
of the three possible input files: counts, experimental conditions,
gene lengths) and file options have effect on the section they are
specified. By default the first section is that of counts. Besides
specifying the input file, the options `-fc`, `-fs` and `-fl` cause 
the current section to change to respectively counts, experiments
experimental conditions and gene lengths with effects on the
subsequent options. 
Some special options are `-sc`, `-ss` and `-sl`. They are available
only for excel (*xls* and *xlsx*) files and allow to specify the sheet
name within the file. These options change the current section as
well. If the corresponding file was not specified they inherit that of the
previous section. For example if the file *myexperiment.xlsx* contains
three separate sheets (namely counts, experiments and lengths), you can use
the following shortcut

`DeClUt.py -fc myexperiment.xlsx -sc counts -ss experiments -sl
lengths`

in place of the command

`DeClUt.py -fc myexperiment.xlsx -sc counts -fs myexperiment.xlsx -ss
experiments -fl myexperiment.xlsx -sl lengths`

The section count has options to use when either experimental
conditions or gene lengths are stored in the same file of the counts
as rows/columns of the count matrix. Notice that, in this case, DeClUt
supposes experimental conditions to have the same orientation of
samples and gene lengths to have the same orientation of genes. 
The option `--sampletype` allows to specify the
index of the row/column of the experimental conditions while
`--genelength` does the same for gene lengths. The row/column index is
specified similarly to a spreadsheet and can be indifferently a number
(starting to 1) or a letter in the range [A -- ZZ]. 

The use of the option  `--sampletype` within the counts section makes
the experimental conditions to be read from the the same file of counts
(specified via `-fc`). Consequently,  the option `-fs`  becomes not
necessary and should not be used. Similarly, `--genelength` within the
counts section cause gene lengths to be read from the same file of
counts making the use of `-fl` not necessary.  The rows and columns
specified with `--sampletype` and `--genelength` are excluded reading
the counts matrix.

For example consider the file below (let call it data.csv)


|     |    A   |    B   |     C    |     D    | ... |     N    |
|-----|:------:|:------:|:--------:|:--------:|:---:|:--------:|
| 1   | Gene   | Length | Sample 1 | Sample 2 | ... | Sample n |
| 2   |        |        | Control  | Control  | ... | Test     |
| 3   | Gene 1 | 10     | 234      | 234      | ... | 543      |
| 4   | Gene 2 | 50     | 5234     | 43       | ... | 42       |
| ... | ...    | ...    | ...      | ...      | ... | ...      |
| m   | Gene m | 300    | 2524     | 254      | ... | 245      |

Counts, experimental conditions and gene lengths can be read at once
with the following command:

`DeClUt.py -fc data.csv --sampletype 2 --genelength B`

In this case the counts matrix will contain data from column C to
column N and from Row 3 to row m.


# Authors

- Mario Zanfardino, Monica Franzese, IRCCS Synlab SDN, Via E. Gianturco, 113, Naples, 80143, Italy
- Filippo Geraci Institute for Informatics and Telematics, CNR, Via G. Moruzzi 1, Pisa, 56124, Italy

# Reference

**DeClUt: decluttering differentially expressed genes through clustering
of their expression profiles**

*Mario Zanfardino, Monica Franzese, Filippo Geraci*

Computer Methods and Programs in Biomedicine

Volume 254, September 2024

