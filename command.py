
import argparse
import sys

class params:
    def __init__ (self):
        self.filename = None
        self.filetype = None
        self.sheet = None
        self.need_to_transpose = None  # Will guess if it is necessary
        self.length_index = None       #  if gene length is in the file this is the row/column index
        self.class_index = None        #  if target is in the file this is the row/column index
        self.skip_rows = None          #  0-based index of the rows to skip
        self.skip_columns = None       #  0-based index of the columns to skip
        self.gene_index = None         #  index of the row/column with gene name - if any
        self.sample_index = None       #  index of the row/column with sample name - if any
        self.has_header = True         #  Used to check if gene lengths or sample type tables have header
        self.last_file_section_id = None   #  Used for xls(x) files if sheet is specified and file is not
        
    #  Add default parameters for the section on a partially filled structure
    def complete_defaults_counts (self):
        if self.gene_index is None:
            self.gene_index = 1
        if self.sample_index is None:
            self.sample_index = 1
        if self.skip_rows is None:
            self.skip_rows = []
        if self.skip_columns is None:
            self.skip_columns = []
            
    def complete_defaults_lengths (self):
        #  sample_index should be None in this case
        if self.has_header is True:  #   This is used to skip the header - robust to transpose
            self.sample_index = 1   
        if self.gene_index is None:
            self.gene_index = 1
        if self.length_index is None:
            self.length_index = 2
        if self.skip_rows is None:
            self.skip_rows = []
        if self.skip_columns is None:
            self.skip_columns = []
            
    def complete_defaults_samples (self):
        #  gene_index should be None in this case
        if self.has_header is True:  #   This is used to skip the header - robust to transpose
            self.gene_index = 1
        if self.need_to_transpose is None:
            self.need_to_transpose = True
        if self.sample_index is None:
            self.sample_index = 1
        if self.class_index is None:
            self.class_index = 2
        if self.skip_rows is None:
            self.skip_rows = []
        if self.skip_columns is None:
            self.skip_columns = []
        
    def print_params (self, name):
        print (name)
        print ("Filename:    ", self.filename)
        print ("Filetype:    ", self.filetype)
        print ("Sheet:       ", self.sheet)
        print ("Orientation: ", self.need_to_transpose)
        print ("Gene index:  ", self.gene_index)
        print ("Length index:", self.length_index)
        print ("Class index: ", self.class_index)
        print ("Sample index:", self.sample_index)
        print ("Skip rows:   ", self.skip_rows)
        print ("Skip cols:   ", self.skip_columns)
        print ("Has header:  ", self.has_header)
        print ("Ref file:    ", self.last_file_section_id)
        print ()

        
class CustomAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if not 'ordered_args' in namespace:
            setattr(namespace, 'ordered_args', [["section", "counts"]])
        previous = namespace.ordered_args
        if self.dest == "file_counts" or self.dest == "sheet_counts":
            previous.append (["section", "counts"])
        if self.dest == "file_samples" or self.dest == "sheet_samples":
            previous.append (["section", "samples"])
        if self.dest == "file_lengths" or self.dest == "sheet_lengths":
            previous.append (["section", "lengths"])

        previous.append([self.dest, values])
        setattr(namespace, 'ordered_args', previous)
        prev_attr = getattr (namespace, self.dest)
        if prev_attr is None:
            prev_attr = []
        if not isinstance (prev_attr, list):
            prev_attr = [prev_attr]
        prev_attr.append (values)
        setattr (namespace, self.dest, prev_attr)


class command_line:
    def __init__ (self):
        self.sections = [params () for _ in range (3)]  #  0 counts, 1 experiments, 2 lengths
        self.normalization = None
        self.round = None
        self.outfile = None
        self.__setup_parser ()

    
    def __setup_parser (self):        
        self.parser = argparse.ArgumentParser(prog='DeClUt', description='DeClut differential expression program.', epilog="Written by Filippo Geraci\nReport bugs to: filippo.geraci@iit.cnr.it\nIf you find this software useful please cite:\n * DeClUt: decluttering differentially expressed genes through clustering of their expression profiles\n   Mario Zanfardino, Monica Franzese, Filippo Geraci\n   Computer Methods and Programs in Biomedicine\n   Volume 254, September 2024", formatter_class=argparse.RawDescriptionHelpFormatter)
        self.parser.add_argument('-fc', '--file-counts',  help="filename of the table with counts", action=CustomAction)
        self.parser.add_argument('-sc', '--sheet-counts',  help="name of the sheet of the table with count if file format is excel (ignored for CSV or TSV)", action=CustomAction)
        self.parser.add_argument('-fs', '--file-samples',  help="filename of the table with sample/experiment", action=CustomAction)
        self.parser.add_argument('-ss', '--sheet-samples',  help="name of the sheet of the table with sample/experiment if file format is excel (ignored for CSV or TSV)", action=CustomAction)
        self.parser.add_argument('-fl', '--file-lengths',  help="filename of the table with gene lengths", action=CustomAction)
        self.parser.add_argument('-sl', '--sheet-lengths',  help="name of the sheet of the table with gene lengths if file format is excel (ignored for CSV or TSV)", action=CustomAction)

        self.parser.add_argument('-O', '--orientation', choices=['bygene', 'bysample'], help="specify if the table stores genes or samples in the rows (if not specified the software guesses the orientation assuming that genes are more than samples)", action=CustomAction)
        self.parser.add_argument('--skiprows', '--skip-rows', nargs='+', help="space separated list of positions of the rows to skip (either as 1-base number or excel like letters)",action=CustomAction, metavar="ROW_INDICES")
        self.parser.add_argument('--skipcolumns', '--skip-columns', nargs='+', help="space separated list of positions of the columns to skip (either as 1-base number or excel like letters)", action=CustomAction, metavar="COLUMN_INDICES")
        self.parser.add_argument('-g', '--genename', help="position of the row/column with gene name (either as 1-base number or excel like letters) default (A)", action=CustomAction, metavar="INDEX")
        self.parser.add_argument('-l', '--genelength', help="position of the row/column with gene length (either as 1-base number or excel like letters) default (B)", action=CustomAction, metavar="INDEX")
        self.parser.add_argument('-s', '--samplename', help="position of the row/column with the sample name (either as 1-base number or excel like letters) default (1)", action=CustomAction, metavar="INDEX")
        self.parser.add_argument('-t', '--sampletype', help="position of the row/column with the sample type (either as 1-base number or excel like letters) default (2)", action=CustomAction, metavar="INDEX")
        self.parser.add_argument('-H', '--no-header', nargs=0, help="tells the software that first row of the table contains a header (this option makes sense only in lengths table and experiment table)", action=CustomAction)

        self.parser.add_argument('-n', '--normalization', choices=["NO", "CPM", "RPKM", "FPKM", "TPM", "MOR", "UQ", "MQ", "TMM"], default="NO", help="Normalization algorithm (default: no normalization)")
        self.parser.add_argument('--threshold-function', choices=["DD", "PM", "MS", "DM", "DMM"], default="DMM", help="Function used by the clustering algorithm to determine the initial threshold. Options are: DD (diametral distance), PM (point mean), MS (mean shift), DM (diametral mean), DMM (diametral median). (default: DMM)")
        self.parser.add_argument('-q', '--quiet', action='store_true', help="Disable progress bar visualization while running DeClUt")        
        self.parser.add_argument('-r', '--round', action='store_true', help="Round counts to integers after normalization (default: no)")
        self.parser.add_argument('-o', '--outfile', help="Output file (default: standard output)")        
        self.parser.add_argument('--no-out-header', action='store_false', help="Skip header in the Output")        
        self.parser.add_argument('-v', '--verbose-output', action='store_true', help="Verbose output")        
        self.parser.add_argument('-S', '--sort', help="Sort output file by fields (defoult -S F1 -S dispersion)", choices=["F1", "dispersion", "LFC", "ABSLFC"], action='append')
        self.parser.add_argument('-F', '--filter', help="filter output removing genes under threshold for the specified fields ",  action='append', nargs=2, metavar=('{F1,dispersion,ABSLFC}', 'threshold'))
        self.parser.add_argument('--version', action='version', version='%(prog)s 1.0')

    def print_error (self, msg):
        self.__print_error (None, msg)
        
    def __print_error (self, parameter, msg):
        self.parser.print_usage ()
        if parameter is not None:
            print (self.parser.prog + ": error: argument", parameter + ":", msg)
        else:
            print (self.parser.prog + ": error:", msg)
        sys.exit (-1)

    def __post_process_params (self):
        #  for xls(x) file associate the sheet to the file of the previous section
        #  XXX  if -sc is not preceeded by either -fc -fs or -fl the fc file will not be determined
        for i in range (3):
            if self.sections[i].filename is None and self.sections[i].last_file_section_id is not None:
                self.sections[i].filename = self.sections[self.sections[i].last_file_section_id].filename
                self.sections[i].filetype = self.sections[self.sections[i].last_file_section_id].filetype
        if self.sections[0].filename is None:
            self.__print_error ("-fc/-sc", "Either count file not specified or impossible to be desumed from parameters.")
        if self.sections[0].filetype == "csv" and self.sections[0].sheet is not None:
            self.__print_error ("-sc", "It seems that sheet name was specified for not-excel file in section counts")
        if self.sections[1].filetype == "csv" and self.sections[0].sheet is not None:
            self.__print_error ("-ss", "It seems that sheet name was specified for not-excel file in section samples")
        if self.sections[2].filetype == "csv" and self.sections[0].sheet is not None:
            self.__print_error ("-sl", "It seems that sheet name was specified for not-excel file in section lengths")


    def __check_consistency_errors (self):
        class_set = False
        for i in range (3):
            if self.sections[i].filename is not None and self.sections[i].class_index is not None:
                class_set = True
                break
        if not class_set:
            self.__print_error ("-t", "Sample conditions not specified")
        if self.normalization in ["NO","CPM","MOR","UQ","MQ"]: # These do not require gene length if specified raise a error
            for i in range (3):
                #  True if gene length was specified in command line
                if self.sections[i].filename is not None and self.sections[i].length_index is not None:
                    self.__print_error ("-g/-fl/-sl", "Gene length specified with incompatible normalization: \'" + self.normalization + "\'")


    def __setParam (self, argument, arg_name, section_id, section_name, field_name, validity_range):
        if argument[0] != arg_name:
            return
        if not section_id in validity_range:
            self.__print_error (argument[0], "Parameter not valid in section " + section_name)
        previous_param = getattr (self.sections[section_id], field_name)
        if previous_param != None:
            self.__print_error (argument[0], "Parameter specified more than once in section " + section_name)
        if isinstance (argument[1], list):
            if len (argument[1]) == 1:  #  Comma separated in command line
                value = argument[1][0].split (",")
            else:
                value = [elem for elem in argument[1]]
            for i in range (len (value)):
                try:
                    value[i] = int (value[i])
                except:
                    if len (value[i]) > 2:
                        self.__print_error (argument[0], "Value \'" + value[i] + "\' out of range in section " + section_name)
                    value[i] = value[i].upper()
        else:
            try:
                value = int (argument[1])
            except:
                if len (argument[1]) > 2:
                    self.__print_error (argument[0], "Value \'" + argument[1] + "\' out of range in section " + section_name)
                value = argument[1].upper()
                    
        setattr (self.sections[section_id], field_name, value)


    def __check_filter_param (self):
        filter_args = []
        if self.args.filter is None:
            return filter_args
        for arg in self.args.filter:
            if arg[0] not in ["F1","dispersion","ABSLFC"]:
                self.__print_error ("-F/--filter", "invalid choice: \'" + arg[0] + "\' (choose from \'F1\', \'dispersion\', \'ABSLFC\')")
            if arg[1].isdecimal ():
                arg[1] = float (arg[1])
            elif arg[1][0].isdecimal () and arg[1][1:].replace ('.','',1).isdecimal ():
                arg[1] = float (arg[1])
            else:
                self.__print_error ("-F/--filter", "invalid choice for field: \'" + arg[0] + ". Threshold must be float")
            if arg[0] in ["F1","dispersion"]:
                if arg[1] > 1:
                    self.__print_error ("-F/--filter", "invalid choice for field: \'" + arg[0] + ". Threshold must be lower than 1")
                    
            filter_args.append ([arg[0], arg[1]])
        return filter_args

        
    #  These checks are not about command line
    def check_file_content (self, M, gene_labels, class_labels, gene_lengths):
        if len (M) == 0:
            self.__print_error (None, "Count matrix is empty")
        if len (gene_labels) == 0:
            self.__print_error (None, "Gene name not specifyed")
        if len (class_labels) == 0:
            self.__print_error (None, "Sample conditions not specifyed")
        if len (M) != len (gene_labels):
            self.__print_error (None, "The number of genes in the count matrix (" + str (len (M)) + ") and the number of genes (" + str (len (gene_labels)) + ") do not match")
        if len (gene_lengths) != 0:  #  otherwise no gene length was specified
            if len (M) != len (gene_lengths):
                self.__print_error (None, "The number of genes in the count matrix (" + str (len (M)) + ") and the number of gene lengths (" + str (len (gene_lengths)) + ") do not match")
            # Check that values in gene_lengths are numbers
            for gene in gene_lengths:
                if not (isinstance (gene, int) or isinstance (gene, float)):
                    self.__print_error (None, "Gene lengths contain a non-numerical value")
                else:
                    if gene == 0:
                        self.__print_error (None, "Gene length cannot be 0bp")
                
        if len (M[0]) != len (class_labels):
            self.__print_error (None, "The number of samples in the count matrix (" + str (len (M[0])) + ") and the number of experimental conditions (" + str (len (class_labels)) + ") do not match")
        exp_conditions = len (set (class_labels))
        if exp_conditions != 2:
            self.__print_error (None, "DE analysis requires 2 experimental conditions " + str (exp_conditions) + " found: " + \
                                ",".join ([str (elem) for elem in set (class_labels)]))

        # Check that values in M are numbers
        for i in range (len (M)):
            for j in range (len (M[i])):
                if not (isinstance (M[i][j], int) or isinstance (M[i][j], float)):
                    self.__print_error (None, "Counts matrix contains a non-numerical value")

            
    def parse_args (self):
        self.args = self.parser.parse_args()
        section_id = 0
        last_file_section_id = 0   #  The section id of the last -fc -fs -fl invocation
        if not 'ordered_args' in self.args:
            self.__print_error ("-fc/-fs/-fl", "No input filename specified")
        for argument in self.args.ordered_args:
            if argument[0] == "section":
                if argument[1] == "counts":
                    section_id = 0
                if argument[1] == "samples":
                    section_id = 1
                if argument[1] == "lengths":
                    section_id = 2
                section_name = argument[1]
                
            if argument[0] == "file_counts" or argument[0] == "sheet_counts":
                    section_id = 0
                    section_name = "counts"
            if argument[0] == "file_samples" or argument[0] == "sheet_samples":
                    section_id = 1
                    section_name = "samples"
            if argument[0] == "file_lengths" or argument[0] == "sheet_lengths":
                    section_id = 2
                    section_name = "lengths"

            if argument[0][:5] == "file_":
                if self.sections[section_id].filename is not None:
                    self.__print_error (argument[0], "Filename specified more than once in section " + section_name)
                self.sections[section_id].filename = argument[1]
                last_file_section_id = section_id
                a = argument[1].split (".")
                if len (a) == 1:
                    self.sections[section_id].filetype = "csv";
                else:
                    if a[-1] == "xlsx":
                        self.sections[section_id].filetype = "xlsx"
                    elif a[-1] == "xls":
                        self.sections[section_id].filetype = "xls"
                    else:
                        self.sections[section_id].filetype = "csv"

            if argument[0][:6] == "sheet_":
                if self.sections[section_id].sheet is not None:
                    self.__print_error (argument[0], "Sheet name specified more than once in section " + section_name)
                self.sections[section_id].sheet = argument[1]
                #  This is used to retrieve current file name if not specified
                self.sections[section_id].last_file_section_id = last_file_section_id
                
            self.__setParam (argument, "samplename", section_id, section_name, "sample_index", [0,1])
            self.__setParam (argument, "genename", section_id, section_name, "gene_index", [0,2])
            self.__setParam (argument, "genelength", section_id, section_name, "length_index", [0,2])
            self.__setParam (argument, "sampletype", section_id, section_name, "class_index", [0,1])
            self.__setParam (argument, "skiprows", section_id, section_name, "skip_rows", [0,1,2])
            self.__setParam (argument, "skipcolumns", section_id, section_name, "skip_columns", [0,1,2])

            if argument[0] == "orientation":
                if argument[1] == "bygene":
                    self.sections[section_id].need_to_transpose = False
                if argument[1] == "bysample":
                    self.sections[section_id].need_to_transpose = True

            if argument[0] == "no_header":
                if self.sections[section_id].has_header is False:
                    self.__print_error (argument[0], "Parameter specified more than once in section " + section_name)
                if section_id == 0:  #   Counts
                     self.__print_error (argument[0], "Parameter not valid for section " + section_name)
                self.sections[section_id].has_header = False
            
        self.sections[0].complete_defaults_counts ()
        self.sections[1].complete_defaults_samples ()
        self.sections[2].complete_defaults_lengths ()
        self.__post_process_params ()
        self.normalization = self.args.normalization
        self.round = self.args.round
        self.outfile = self.args.outfile
        self.no_out_header = self.args.no_out_header
        self.quiet = self.args.quiet
        self.verbose_output = self.args.verbose_output
        self.threshold_function = self.args.threshold_function
        if self.args.sort is None:
            self.sort = ["F1", "dispersion"]
        else:
            self.sort =	[elem for elem in self.args.sort]
        self.filter = self.__check_filter_param ()
        self.__check_consistency_errors ()
        #self.sections[0].print_params ("Counts")
        #self.sections[1].print_params ("Samples")
        #self.sections[2].print_params ("Lengths")
        #print (self.args)

#  Used to generate man page with argparse-manpage
def get_parser ():
    cl = command_line ()
    return cl.parser
# argparse-manpage --pyfile ./command.py  --function get_parser --manual-title "DeClUt man page" > declut.1
